FIXME-GFX(Krey): Header here
<img src="../img/header-image.png" width="100%"/>

[![Built with cargo-make](https://sagiegurari.github.io/cargo-make/assets/badges/cargo-make.svg)](https://sagiegurari.github.io/cargo-make)

Repository creating devuan templateVM package for various distributions providing domain-0 (dom0) on QubesOS with priority for the default fedora.

### Goal
Get devuan templateVM on QubesOS

### Abstract
1. Create the devuan userland with scripts to put it in TemplateVM of QubesOS
2. Package it
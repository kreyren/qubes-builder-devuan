FIXME-GFX(Krey): Add header image

# Code indexing (tags)
Source code in this project is adapted for rolling release with versioned releases and as such the code requires to be indexed for continous improvements that were captured during a quality assurance.

You are encouraged to search the source code and look for these tags to resolve them.

## Resolving FIXME tags
Source code in this project has comments above an imperfect code that needs resolving which is sorted in follwing categories:
- FIXME = General fixme
- FIXME-QA = Fixme concerning Code quality
- FIXME-AUTO = Fixme concerning automatization such as Continuous integrationg
- FIXME-TRANSLATE = Codeblocks that return an output and need translating in different spoken language
- FIXME-CROSS = Codeblocks that needs implementation for cross-platform
- FIXME-GFX = Tags for graphics-releted concerns
- FIXME-<EDITOR> = Tags concerning configuration of the development environment

Example of FIXME tag:
```sh
# FIXME-QA(Krey): Complaining about things
echo something
```

## Resolving DNM (DoNotMerge) tags
This repository is manage on rolling-release with versioned releases, the main branch might contain `DNM` tags in the source code for things that needs to be resolved prior to merging that should be resolved with priority. These tags share categories with FIXME tags.

These tags prevent versioned release.

# Peer-Review
Any relevant information concerning the implementation is appreciated.